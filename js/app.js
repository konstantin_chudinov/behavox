define(function (require) {
    var drawDiagram = require('./spyder-diagram/spyder-diagram-draw');

    d3.json("js/relations.json", function (error, json) {
        if (error) {
            throw error;
        } else {
            drawDiagram.draw(json.currentPerson, json.relations);
        }
    });
});