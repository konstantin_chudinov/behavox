define(function (require) {
    var collisionAlg = require('./collision-alg');
    var geometryService = require('../util/geometry-service');
    var rootNode;
    var circleList = [];
    var MAX_WEIGHT_VALUE = 99999999;
    var TWO_PI = 360.0;
    var MINUS_TWO_PI = -360.0;

    var scaleConfig = {
        radiusScale: function (value) {return value;},
        distanceToCenterScale: function (value) {return value;},
        severityScale: function (value) {return value;}
    };

    /* Class ArrangedNode */
    var circleArrangedNode = function (loAngle, hiAngle, person) {
        var circle = geometryService.circle({
            angle: loAngle + (hiAngle - loAngle) / 2,
            radius: scaleConfig.radiusScale(person.collaborationAmount),
            distance: scaleConfig.distanceToCenterScale(person.closeness)
        });
        circle.label = person.name;
        circle.lo = loAngle;
        circle.hi = hiAngle;
        circle.weight = 1;
        circle.relationSeverity = scaleConfig.severityScale(person.relationSeverity);
        return circle;
    };

    /* Class Dummy */
    var dummyArrangedNode = function () {
        return {weight: MAX_WEIGHT_VALUE}
    };

    var arrangeItem = function (item) {
        if (!rootNode) {
            rootNode = circleArrangedNode(MINUS_TWO_PI, TWO_PI, item);
            rootNode.clockWiseNode = dummyArrangedNode(); // Only once, it is needed to not consider [0,-360] arc
            rootNode.weight = 1;
            return rootNode;
        } else {
            var insertedValue = tryToInsertInto(rootNode, item);
            if (insertedValue) {
                return insertedValue;
            } else {
                console.log('Missed person: '); //Just for debug purpose. Error handling is out of scope
                console.log(item);
                throw 'Person is missed';
            }
        }
    };

    var tryToInsertInto = function (node, item) {
        if (canUseThisLevel(node)) {
            return tryToPutAtThisLevel(node, item);
        }
        var inserted = null;

        if (firstArgIsMorePreferable(node.clockWiseNode, node.counterClockWiseNode)) {
            inserted = tryToInsertInto(node.clockWiseNode, item);
            if (!inserted) {
                inserted = tryToInsertInto(node.counterClockWiseNode, item);
            }
        } else {
            inserted = tryToInsertInto(node.counterClockWiseNode, item);
            if (!inserted && !isFirstDummyLevel(node)) {
                inserted = tryToInsertInto(node.clockWiseNode, item);
            }
        }
        if (inserted) {
            increaseWeight(node, inserted);
        }
        return inserted;
    };

    var tryToPutAtThisLevel = function (node, item) {
        var mid = findMiddle(node);
        //At first, try to insert into right subnode
        if (!node.clockWiseNode) {
            var createdNode = circleArrangedNode(node.lo, mid, item);
            if (!collisionAlg.intersects(createdNode, {rootNode: rootNode, nodeList: circleList})) {
                node.clockWiseNode = createdNode;
                increaseWeight(node, createdNode);
                return node.clockWiseNode;
            }
        }
        //Else if we failed inserting into right subnode, then try to insert into left one.
        if (!node.counterClockWiseNode) {
            var createdNode = circleArrangedNode(mid, node.hi, item);
            if (!collisionAlg.intersects(createdNode, {rootNode: rootNode, nodeList: circleList})) {
                node.counterClockWiseNode = createdNode;
                increaseWeight(node, createdNode);
                return node.counterClockWiseNode;
            }
        }
        return null;
    };

    var canUseThisLevel = function (node) {
        return !node.clockWiseNode || !node.counterClockWiseNode;
    };

    var firstArgIsMorePreferable = function (firstNode, secondNode) {
        return firstNode.weight < secondNode.weight;
    };

    var isFirstDummyLevel = function (node) {
        return node.clockWiseNode.weight === MAX_WEIGHT_VALUE;
    };

    var increaseWeight = function (parentNode, childNode) { //May be it's better to consider
        parentNode.weight++;
    };

    var findMiddle = function (node) {
        var mid = node.lo + (node.hi - node.lo) / 2.0;
        if (mid === TWO_PI) {
            mid = 0.0;
        }
        return mid;
    };

    return {
        setScaleConfiguartion: function (newScaleConfig) {
            scaleConfig = newScaleConfig;
        },

        clearState: function () {
            circleList = [];
            rootNode = null;
        },

        arrangeRelationList: function (relationList) {
            for (var i = 0; i < relationList.length; i++) {
                var arrangedItem = arrangeItem(relationList[i]);
                if (arrangedItem) {
                    circleList.push(arrangedItem);
                }
            }
            return circleList;
        }
    };
});