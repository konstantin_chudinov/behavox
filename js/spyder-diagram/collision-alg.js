define(function () {
    return {
        intersects: function(node, context) {
            if (context.nodeList) {
                return node.intersectAny(context.nodeList);
            }
            return true;
        }
    };
});