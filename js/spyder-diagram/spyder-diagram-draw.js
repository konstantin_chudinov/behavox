define(function (require) {
    var circleLayout = require('./circle-layout-alg');
    var geometryService = require('../util/geometry-service');
    var SHOW_LABEL_MIN_RADIUS = 80;


    //Scale config
    var dynamicRadiusScale = 5;
    var distanceToCenterScale = d3.scale.linear()
        .domain([0, 100])
        .range([40, 400]);

    var severityScale = d3.scale.linear()
        .domain([0, 100])
        .range([0, 100]);

    var radiusScaleFor = function (dynamicRatio) {
        return d3.scale.linear()
            .domain([0, 100])
            .range([dynamicRatio, dynamicRatio * 10])
    };

    var radiusScale = radiusScaleFor(dynamicRadiusScale);

    //May be it's better to predict dynamicRadiusScale using SUM(circle.area) and COUNT(circle).
    //Anyway, I think this is not part of exercise
    var scaleWhileCannotArrange = function (attemptFunc) {
        var error = true;
        while (error) {
            try {
                circleLayout.setScaleConfiguartion({
                    radiusScale: radiusScale,
                    distanceToCenterScale: distanceToCenterScale,
                    severityScale: severityScale
                });
                attemptFunc();
                error = false;
            } catch (ex) {
                if (ex === 'Person is missed') {
                    dynamicRadiusScale /= 2;
                    radiusScale = radiusScaleFor(dynamicRadiusScale);
                    circleLayout.clearState();
                } else {
                    throw ex;
                }
            }
        }
    };

    return {
        draw: function (currentPerson, personList) {
            var circleList = [];
            scaleWhileCannotArrange(function() {
                circleList = circleLayout.arrangeRelationList(personList);
            });

            var diameter = 960;

            var svg = d3.select('body').append('svg')
                .attr('width', diameter)
                .attr('height', diameter)
                .append('g')
                .attr('transform', 'translate(' + diameter / 2 + ',' + diameter / 2 + ')');


            var circle = svg.selectAll('circle.person')
                .data(circleList);

            circle.exit().remove();

            var g = circle.enter();
            g.append('line')
                .attr('class', 'relation')
                .attr('x1', 0)
                .attr('y1', 0)
                .attr('x2', function(item) { return geometryService.center(item).x; })
                .attr('y2', function(item) { return geometryService.center(item).y; });

            g.append('circle')
                .attr('class', 'person')
                .attr('fill', function(item) { return d3.hcl(-97, 32, item.relationSeverity);})
                .attr('cx', function(item) { return geometryService.center(item).x; })
                .attr('cy', function(item) { return geometryService.center(item).y; })
                .attr('r', function(item) { return item.radius; });

            g.append('text')
                .attr('x', function(item) {return geometryService.center(item).x;})
                .attr('y', function(item) {return geometryService.center(item).y - item.radius - 2;})
                .text(function(item) {return (radiusScale(SHOW_LABEL_MIN_RADIUS) < item.radius) ?  item.label : '';});

            svg.append('circle')
                .attr('class', 'center')
                .attr('cx', 0)
                .attr('cy', 0)
                .attr('r', '12');

            svg.append('text')
                .attr('x', 0)
                .attr('y', -14)
                .text(currentPerson.name);
        }
    };
});