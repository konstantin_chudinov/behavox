define(function () {
    var circleCenter = function (circle) {
        return {
            x: circle.distance * Math.cos(circle.angle / 180 * Math.PI),
            y: circle.distance * Math.sin(circle.angle / 180 * Math.PI)
        };
    };

    return {
        center: circleCenter,

        circle: function (node) {
            return {
                distance: node.distance,
                radius: node.radius,
                angle: node.angle,

                intersect: function (other) {
                    var currentCenter = circleCenter(this);
                    var otherCenter = circleCenter(other);
                    var distanceBetweenCircles = Math.sqrt(Math.pow(currentCenter.x - otherCenter.x, 2) + Math.pow(currentCenter.y - otherCenter.y, 2));
                    return distanceBetweenCircles < this.radius + other.radius;
                },

                intersectAny: function (circleArray) {
                    for (var i = 0; i < circleArray.length; i++) {
                        if (this.intersect(circleArray[i])) {
                            return true;
                        }
                    }
                    return false;
                }
            }
        }
    };
});
